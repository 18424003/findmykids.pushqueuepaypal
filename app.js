const cors = require("cors");
const createError = require("http-errors");
const express = require("express");
const morgan = require("morgan");
require("express-async-errors");
const app = express();
app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

app.use("/push", require("./routes/pushQueue.route"));

// app.use((req, res, next) => {
//   throw createError(404, "No resources found!");
// });

// app.use(function (err, req, res, next) {
//   console.log("err, req, res, next", err);
//   if (typeof err.status === "undefined" || err.status === 500) {
//     res.status(500).send("View error log on console screen!");
//   } else {
//     res.status(err.status).send(err);
//   }
// });

app.set("PORT", process.env.PORT || 5007);

app.listen(app.get("PORT"), () => {
  console.log("App is listening on port 5007");
});