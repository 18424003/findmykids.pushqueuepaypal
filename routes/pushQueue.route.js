const pushQueueModel = require("../models/pushQueue.model");
const verifyCallFromPayPalModel = require("../models/verifyCallFromPaypal.model");
const express = require("express");
const router = express.Router();
const { v4: uuidv4 } = require("uuid");
const { json } = require("express");

router.get("/", async (req, res) => {
  res.send({
    'x': 'x'
  })
});

router.post("/", async (req, res) => {
  if (
    req.headers["paypal-transmission-id"] === undefined ||
    req.headers["paypal-transmission-time"] === undefined ||
    req.headers["paypal-cert-url"] === undefined ||
    req.headers["paypal-auth-algo"] === undefined ||
    req.headers["paypal-transmission-sig"] === undefined ||
    (req.body.constructor === Object && Object.keys(req.body).length === 0)
  ) {
    res.statusCode = 400;
    res.data = { "message-sent": false };
    return res
      .status(res.statusCode || 400)
      .send({ status: false, response: res.data });
  }
  const result = await verifyCallFromPayPalModel.verifyCallFromPayPal(
    req.headers,
    req.body
  );

  console.log("result", result)

  if (result === "SUCCESS") {
    const subscription = {
      sub_id: req.body.resource.id,
      email: req.body.resource.subscriber.email_address,
    };
    const key = uuidv4();
    const keyUpper = key.toUpperCase();
    const keyFinal =
      keyUpper.substring(0, 4) +
      "-" +
      keyUpper.substring(4, 8) +
      "-" +
      keyUpper.substring(9, 23) +
      "-" +
      keyUpper.substring(24, 28) +
      "-" +
      keyUpper.substring(28, 32) +
      "-" +
      keyUpper.substring(32);
    subscription["key"] = keyFinal;
    const date = Math.round(+new Date() / 1000);
    console.log("date", date)
    subscription["date"] = date;
    await pushQueueModel.publishToQueue("Subscription", subscription);

    

    res.statusCode = 200;
    console.log("res.statusCode", res.statusCode)
    res.data = { "message-sent": true };
    res
      .status(res.statusCode || 200)
      .send({ status: true, response: res.data });
  } else {
    res.statusCode = 400;
    res.data = { "message-sent": false };
    res
      .status(res.statusCode || 400)
      .send({ status: false, response: res.data });
  }
});
module.exports = router;
