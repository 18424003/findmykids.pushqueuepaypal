require("dotenv").config();
const amqplib = require("amqplib/callback_api");
const CONN_URL = process.env.QUEUE_URI;

let ch = null;
amqplib.connect(CONN_URL, (err, conn) => {
  conn.createChannel((err, channel) => {
    ch = channel;
    // const QUEUE = 'Subscription'
    // channel.assertQueue(QUEUE);
  });
});

module.exports = {
  publishToQueue: async (queueName, data) => {
    ch.sendToQueue(queueName, Buffer.from(JSON.stringify(data)), {
      persistent: true,
    });
  },
};

process.on("exit", (code) => {
  ch.close();
  console.log("Closing rabbitMQ channel");
});
