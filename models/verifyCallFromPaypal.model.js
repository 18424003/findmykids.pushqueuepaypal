const axios = require("axios");

module.exports = {
  verifyCallFromPayPal: async(webhook_headers, webhook_body) => {
    try {
      const {
        data: { access_token },
      } = await axios({
        url: "https://api.sandbox.paypal.com/v1/oauth2/token",
        method: "post",
        headers: {
          Accept: "application/json",
          "Accept-Language": "en_US",
          "content-type": "application/x-www-form-urlencoded",
        },
        auth: {
          username:
            "AZougqOyKucRxBRb-R6xthxmwf6EyV9PjUUsSgA7BpvLHQ8MQ9JOcygThdXyIjRhXFnOU7uHJzi8INex",
          password:
            "ENAkb0zTWYTSVWueVjfwisrvnYqUHJ-KyqMVC83UGCRtgv5cLJ1kM66O2foK19RuPDx2lWc6T44j9p_o",
        },
        params: {
          grant_type: "client_credentials",
        },
      });
      const token = access_token;
      const result = await axios({
        url:
          "https://api.sandbox.paypal.com/v1/notifications/verify-webhook-signature",
        method: "post",
        headers: {
          Authorization: `bearer  ${token}`,
        },
        data: {
          transmission_id: webhook_headers["paypal-transmission-id"],
          transmission_time: webhook_headers["paypal-transmission-time"],
          cert_url: webhook_headers["paypal-cert-url"],
          auth_algo: webhook_headers["paypal-auth-algo"],
          transmission_sig: webhook_headers["paypal-transmission-sig"],
          webhook_id: "89F95537P5360480K",
          webhook_event: webhook_body,
        },
      });
      return result.data.verification_status
    } catch (error) {
      return "FAILURE";
    }
  }
}